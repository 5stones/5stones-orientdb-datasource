///<reference path='../node_modules/grafana-sdk-mocks/app/headers/common.d.ts' />
System.register(['lodash', './response_parser', './orientdb_query'], function(exports_1) {
    var lodash_1, response_parser_1, orientdb_query_1;
    var OrientDBDatasource;
    return {
        setters:[
            function (lodash_1_1) {
                lodash_1 = lodash_1_1;
            },
            function (response_parser_1_1) {
                response_parser_1 = response_parser_1_1;
            },
            function (orientdb_query_1_1) {
                orientdb_query_1 = orientdb_query_1_1;
            }],
        execute: function() {
            OrientDBDatasource = (function () {
                /** @ngInject */
                function OrientDBDatasource(instanceSettings, backendSrv, templateSrv, $q) {
                    var _this = this;
                    this.backendSrv = backendSrv;
                    this.templateSrv = templateSrv;
                    this.$q = $q;
                    this.interpolateVariable = function (value, variable) {
                        if (typeof value === 'string') {
                            if (variable.multi || variable.includeAll) {
                                return _this.queryModel.quoteLiteral(value);
                            }
                            else {
                                return value;
                            }
                        }
                        if (typeof value === 'number') {
                            return value;
                        }
                        var quotedValues = lodash_1.default.map(value, function (v) {
                            return _this.queryModel.quoteLiteral(v);
                        });
                        return quotedValues.join(',');
                    };
                    this.name = instanceSettings.name;
                    this.id = instanceSettings.id;
                    this.baseUrl = '/api/datasources/proxy/' + this.id + '/orientdb/';
                    this.database = instanceSettings.jsonData.database;
                    this.queryModel = new orientdb_query_1.default({});
                    this.responseParser = new response_parser_1.default(this.$q);
                }
                OrientDBDatasource.prototype._getSqlEndpoint = function () {
                    return this.baseUrl + 'command/' + this.database + '/sql';
                };
                OrientDBDatasource.prototype.query = function (options) {
                    var _this = this;
                    var queries = lodash_1.default.filter(options.targets, function (target) {
                        return target.hide !== true;
                    }).map(function (target) {
                        var queryModel = new orientdb_query_1.default(target, options.range, _this.templateSrv, options.scopedVars);
                        return queryModel.render(_this.interpolateVariable);
                    });
                    if (queries.length === 0) {
                        return this.$q.when({ data: [] });
                    }
                    return this.backendSrv
                        .datasourceRequest({
                        url: this._getSqlEndpoint(),
                        method: 'POST',
                        data: queries[0],
                    })
                        .then(function (results) {
                        return _this.responseParser.processQueryResult(results);
                    });
                };
                OrientDBDatasource.prototype.annotationQuery = function (options) {
                    throw new Error('Annotation Support not implemented yet.');
                };
                OrientDBDatasource.prototype.metricFindQuery = function (query) {
                    throw new Error('Metric Find Support not implemented yet.');
                };
                OrientDBDatasource.prototype.testDatasource = function () {
                    return this.backendSrv
                        .datasourceRequest({
                        url: this._getSqlEndpoint(),
                        method: 'POST',
                        data: 'SELECT 1',
                    })
                        .then(function (res) {
                        return { status: 'success', message: 'Database Connection OK' };
                    })
                        .catch(function (err) {
                        console.log(err);
                        if (err.data && err.data.message) {
                            return { status: 'error', message: err.data.message };
                        }
                        else {
                            return { status: 'error', message: err.status };
                        }
                    });
                };
                return OrientDBDatasource;
            })();
            exports_1("default", OrientDBDatasource);
        }
    }
});
//# sourceMappingURL=datasource.js.map