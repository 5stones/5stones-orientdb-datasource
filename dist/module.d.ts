import OrientDBDatasource from './datasource';
import { OrientDBQueryCtrl } from './query_ctrl';
import { OrientDBConfigCtrl } from './config_ctrl';
declare class OrientDBAnnotationsQueryCtrl {
    static templateUrl: string;
}
export { OrientDBDatasource as Datasource, OrientDBQueryCtrl as QueryCtrl, OrientDBConfigCtrl as ConfigCtrl, OrientDBAnnotationsQueryCtrl as AnnotationsQueryCtrl };
