import { QueryCtrl } from 'app/plugins/sdk';
import OrientDBQuery from './orientdb_query';
export interface QueryMeta {
    sql: string;
}
export declare class OrientDBQueryCtrl extends QueryCtrl {
    private templateSrv;
    private $q;
    private uiSegmentSrv;
    static templateUrl: string;
    formats: any[];
    lastQueryMeta: QueryMeta;
    lastQueryError: string;
    showHelp: boolean;
    queryModel: OrientDBQuery;
    selectMenu: any[];
    /** @ngInject */
    constructor($scope: any, $injector: any, templateSrv: any, $q: any, uiSegmentSrv: any);
    updateProjection(): void;
    onDataReceived(dataList: any): void;
    onDataError(err: any): void;
    transformToSegments(config: any): (results: any) => any;
    handleQueryError(err: any): any[];
}
