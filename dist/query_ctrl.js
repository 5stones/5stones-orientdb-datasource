System.register(['lodash', 'app/plugins/sdk', './orientdb_query'], function(exports_1) {
    var __extends = (this && this.__extends) || function (d, b) {
        for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
    var lodash_1, sdk_1, orientdb_query_1;
    var defaultQuery, OrientDBQueryCtrl;
    return {
        setters:[
            function (lodash_1_1) {
                lodash_1 = lodash_1_1;
            },
            function (sdk_1_1) {
                sdk_1 = sdk_1_1;
            },
            function (orientdb_query_1_1) {
                orientdb_query_1 = orientdb_query_1_1;
            }],
        execute: function() {
            defaultQuery = "SELECT\n  UNIX_TIMESTAMP(<time_column>) as time_sec,\n  <value column> as value,\n  <series name column> as metric\nFROM <table name>\nWHERE $__timeFilter(time_column)\nORDER BY <time_column> ASC\n";
            OrientDBQueryCtrl = (function (_super) {
                __extends(OrientDBQueryCtrl, _super);
                /** @ngInject */
                function OrientDBQueryCtrl($scope, $injector, templateSrv, $q, uiSegmentSrv) {
                    _super.call(this, $scope, $injector);
                    this.templateSrv = templateSrv;
                    this.$q = $q;
                    this.uiSegmentSrv = uiSegmentSrv;
                    this.target = this.target;
                    this.queryModel = new orientdb_query_1.default(this.target, templateSrv, this.panel.scopedVars);
                    this.updateProjection();
                    this.formats = [{ text: 'Time series', value: 'time_series' }, { text: 'Table', value: 'table' }];
                    if (!this.target.rawSql) {
                        // special handling when in table panel
                        if (this.panelCtrl.panel.type === 'table') {
                            this.target.format = 'table';
                            this.target.rawSql = 'SELECT 1';
                            this.target.rawQuery = true;
                        }
                        else {
                            this.target.rawSql = defaultQuery;
                        }
                    }
                    this.panelCtrl.events.on('data-received', this.onDataReceived.bind(this), $scope);
                    this.panelCtrl.events.on('data-error', this.onDataError.bind(this), $scope);
                }
                OrientDBQueryCtrl.prototype.updateProjection = function () {
                };
                OrientDBQueryCtrl.prototype.onDataReceived = function (dataList) {
                    this.lastQueryMeta = null;
                    this.lastQueryError = null;
                    var anySeriesFromQuery = lodash_1.default.find(dataList, { refId: this.target.refId });
                    if (anySeriesFromQuery) {
                        this.lastQueryMeta = anySeriesFromQuery.meta;
                    }
                };
                OrientDBQueryCtrl.prototype.onDataError = function (err) {
                    if (err.data && err.data.results) {
                        var queryRes = err.data.results[this.target.refId];
                        if (queryRes) {
                            this.lastQueryMeta = queryRes.meta;
                            this.lastQueryError = queryRes.error;
                        }
                    }
                };
                OrientDBQueryCtrl.prototype.transformToSegments = function (config) {
                    var _this = this;
                    return function (results) {
                        var segments = lodash_1.default.map(results, function (segment) {
                            return _this.uiSegmentSrv.newSegment({
                                value: segment.text,
                                expandable: segment.expandable,
                            });
                        });
                        if (config.addTemplateVars) {
                            for (var _i = 0, _a = _this.templateSrv.variables; _i < _a.length; _i++) {
                                var variable = _a[_i];
                                var value = void 0;
                                value = '$' + variable.name;
                                if (config.templateQuoter && variable.multi === false) {
                                    value = config.templateQuoter(value);
                                }
                                segments.unshift(_this.uiSegmentSrv.newSegment({
                                    type: 'template',
                                    value: value,
                                    expandable: true,
                                }));
                            }
                        }
                        if (config.addNone) {
                            segments.unshift(_this.uiSegmentSrv.newSegment({ type: 'template', value: 'none', expandable: true }));
                        }
                        return segments;
                    };
                };
                OrientDBQueryCtrl.prototype.handleQueryError = function (err) {
                    this.error = err.message || 'Failed to issue metric query';
                    return [];
                };
                OrientDBQueryCtrl.templateUrl = 'partials/query.editor.html';
                return OrientDBQueryCtrl;
            })(sdk_1.QueryCtrl);
            exports_1("OrientDBQueryCtrl", OrientDBQueryCtrl);
        }
    }
});
//# sourceMappingURL=query_ctrl.js.map