///<reference path="../node_modules/grafana-sdk-mocks/app/headers/common.d.ts" />
System.register([], function(exports_1) {
    var OrientDBConfigCtrl;
    return {
        setters:[],
        execute: function() {
            OrientDBConfigCtrl = (function () {
                function OrientDBConfigCtrl($scope) {
                }
                OrientDBConfigCtrl.prototype.refreshToken = function () {
                    this.current.jsonData.token = btoa(this.current.jsonData.user + ':' + this.current.jsonData.password);
                    console.log(this.current.jsonData.user, this.current.jsonData.password, this.current.jsonData.token);
                };
                OrientDBConfigCtrl.templateUrl = 'partials/config.html';
                return OrientDBConfigCtrl;
            })();
            exports_1("OrientDBConfigCtrl", OrientDBConfigCtrl);
        }
    }
});
//# sourceMappingURL=config_ctrl.js.map