export default class OrientDBQuery {
    target: any;
    range: any;
    templateSrv: any;
    scopedVars: any;
    /** @ngInject */
    constructor(target: any, range?: any, templateSrv?: any, scopedVars?: any);
    quoteLiteral(value: any): string;
    escapeLiteral(value: any): string;
    interpolateQueryStr(value: any, variable: any, defaultFormatFn: any): any;
    render(interpolate?: any): any;
    private _parseMacros(target, scopedVars);
}
