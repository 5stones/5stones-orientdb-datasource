declare class ResponseParser {
    private $q;
    constructor($q: any);
    processQueryResult(res: any): {
        data: any[];
    };
    getSeriesMap(row: any): any;
    appendPointToSeries(point: any, series: any, seriesMap: any): void;
}
export default ResponseParser;
