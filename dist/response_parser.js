System.register([], function(exports_1) {
    var ResponseParser;
    return {
        setters:[],
        execute: function() {
            ResponseParser = (function () {
                function ResponseParser($q) {
                    this.$q = $q;
                }
                ResponseParser.prototype.processQueryResult = function (res) {
                    var data = [];
                    if (!res.data.result) {
                        return { data: data };
                    }
                    var results = res.data.result;
                    var series = {};
                    var seriesMap = null;
                    for (var key in results) {
                        var point = results[key];
                        if (seriesMap === null) {
                            seriesMap = this.getSeriesMap(point);
                        }
                        this.appendPointToSeries(point, series, seriesMap);
                    }
                    for (var seriesKey in series) {
                        var item = series[seriesKey];
                        data.push({
                            target: item['name'],
                            datapoints: item['points'],
                            refId: seriesKey,
                            meta: { rowCount: 0, sql: 'SELECT FROM SjcOrder' },
                        });
                    }
                    return { data: data };
                };
                ResponseParser.prototype.getSeriesMap = function (row) {
                    var map = {};
                    var keys = Object.keys(row);
                    var matched = false;
                    for (var _i = 0; _i < keys.length; _i++) {
                        var key = keys[_i];
                        var metricReg = /(metric)\_(.+)/;
                        var valueReg = /(value)\_(.+)/;
                        if (metricReg.test(key)) {
                            matched = true;
                            var matches = key.match(metricReg);
                            map[matches[2]] = { metricKey: matches[0], valueKey: "value_" + matches[2], timeKey: 'time' };
                        }
                        else if (valueReg.test(key)) {
                            matched = true;
                            var matches = key.match(valueReg);
                            map[matches[2]] = { metricKey: "metric_" + matches[2], valueKey: matches[0], timeKey: 'time' };
                        }
                    }
                    if (!matched) {
                        map['A'] = { metricKey: 'metric', valueKey: 'value', timeKey: 'time' };
                    }
                    return map;
                };
                ResponseParser.prototype.appendPointToSeries = function (point, series, seriesMap) {
                    for (var seriesKey in seriesMap) {
                        var map = seriesMap[seriesKey];
                        var name_1 = point[map['metricKey']];
                        var d = [point[map['valueKey']], point['time']];
                        // don't include any undefined time values
                        if (!d[1]) {
                            continue;
                        }
                        if (series[seriesKey]) {
                            series[seriesKey]['points'].push(d);
                        }
                        else {
                            series[seriesKey] = {
                                name: name_1,
                                points: [
                                    d,
                                ]
                            };
                        }
                    }
                };
                return ResponseParser;
            })();
            exports_1("default",ResponseParser);
        }
    }
});
//# sourceMappingURL=response_parser.js.map