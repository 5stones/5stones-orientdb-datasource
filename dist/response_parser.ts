import _ from 'lodash';

class ResponseParser {
  constructor(private $q) {}

  public processQueryResult(res) {
    let data = [];

    if (!res.data.result) {
      return { data: data };
    }

    let results = res.data.result;
    let series = {};
    let seriesMap = null;

    for (const key in results) {
      const point = results[key];

      if (seriesMap === null) {
        seriesMap = this.getSeriesMap(point);
      }

      this.appendPointToSeries(point, series, seriesMap)
    }

    for(const seriesKey in series) {
      let item = series[seriesKey];
      data.push({
        target: item['name'],
        datapoints: item['points'],
        refId: seriesKey,
        meta: { rowCount: 0, sql: 'SELECT FROM SjcOrder' },
      });
    }

    return { data: data };
  }

  public getSeriesMap(row): any {
    let map = {};
    let keys = Object.keys(row);
    let matched = false;

    for (let key of keys) {
      let metricReg = /(metric)\_(.+)/;
      let valueReg = /(value)\_(.+)/;

      if (metricReg.test(key)) {
        matched = true;
        let matches = key.match(metricReg);
        map[matches[2]] = { metricKey: matches[0], valueKey: `value_${matches[2]}`, timeKey: 'time' };
      } else if (valueReg.test(key)) {
        matched = true;
        let matches = key.match(valueReg);
        map[matches[2]] = { metricKey: `metric_${ matches[2]}`, valueKey: matches[0], timeKey: 'time' };
      }
    }

    if (!matched) {
      map['A'] = { metricKey: 'metric', valueKey: 'value', timeKey: 'time' };
    }

    return map;
  }

  public appendPointToSeries(point, series, seriesMap) {
    for(const seriesKey in seriesMap) {
      const map = seriesMap[seriesKey];

      let name = point[map['metricKey']];
      let d = [point[map['valueKey']], point['time']];

      // don't include any undefined time values
      if (!d[1]) {
        continue;
      }

      if (series[seriesKey]) {
        series[seriesKey]['points'].push(d);
      } else {
        series[seriesKey] = {
          name: name,
          points: [
            d,
          ]
        }
      }
    }
  }
}

export default ResponseParser;
