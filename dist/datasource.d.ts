/// <reference path="../node_modules/grafana-sdk-mocks/app/headers/common.d.ts" />
import ResponseParser from './response_parser';
import OrientDBQuery from './orientdb_query';
export default class OrientDBDatasource {
    private backendSrv;
    private templateSrv;
    private $q;
    id: number;
    name: string;
    baseUrl: string;
    database: string;
    responseParser: ResponseParser;
    queryModel: OrientDBQuery;
    /** @ngInject */
    constructor(instanceSettings: any, backendSrv: any, templateSrv: any, $q: any);
    interpolateVariable: (value: any, variable: any) => any;
    private _getSqlEndpoint();
    query(options: any): any;
    annotationQuery(options: any): void;
    metricFindQuery(query: string): void;
    testDatasource(): any;
}
