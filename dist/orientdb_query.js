System.register(['lodash'], function(exports_1) {
    var lodash_1;
    var OrientDBQuery;
    return {
        setters:[
            function (lodash_1_1) {
                lodash_1 = lodash_1_1;
            }],
        execute: function() {
            OrientDBQuery = (function () {
                /** @ngInject */
                function OrientDBQuery(target, range, templateSrv, scopedVars) {
                    this.target = target;
                    this.range = range;
                    this.templateSrv = templateSrv;
                    this.scopedVars = scopedVars;
                    target.format = target.format || 'time_series';
                    target.timeColumn = target.timeColumn || 'time';
                    target.metricColumn = target.metricColumn || 'none';
                    target.group = target.group || [];
                    target.where = target.where || [{ type: 'macro', name: '$__timeFilter', params: [] }];
                    target.select = target.select || [[{ type: 'column', params: ['value'] }]];
                    // handle pre query gui panels gracefully
                    if (!('rawQuery' in this.target)) {
                        if ('rawSql' in target) {
                            // pre query gui panel
                            target.rawQuery = true;
                        }
                        else {
                            // new panel
                            target.rawQuery = false;
                        }
                    }
                    // give interpolateQueryStr access to this
                    this.interpolateQueryStr = this.interpolateQueryStr.bind(this);
                }
                OrientDBQuery.prototype.quoteLiteral = function (value) {
                    return "'" + value.replace(/'/g, "''") + "'";
                };
                OrientDBQuery.prototype.escapeLiteral = function (value) {
                    return String(value).replace(/'/g, "''");
                };
                OrientDBQuery.prototype.interpolateQueryStr = function (value, variable, defaultFormatFn) {
                    // if no multi or include all do not regexEscape
                    if (!variable.multi && !variable.includeAll) {
                        return this.escapeLiteral(value);
                    }
                    if (typeof value === 'string') {
                        return this.quoteLiteral(value);
                    }
                    var escapedValues = lodash_1.default.map(value, this.quoteLiteral);
                    return escapedValues.join(',');
                };
                OrientDBQuery.prototype.render = function (interpolate) {
                    var target = this.target;
                    if (interpolate) {
                        var sql = this._parseMacros(target, this.scopedVars);
                        return this.templateSrv.replace(sql, this.scopedVars, this.interpolateQueryStr);
                    }
                    else {
                        return target.rawSql;
                    }
                };
                OrientDBQuery.prototype._parseMacros = function (target, scopedVars) {
                    var marcroRegex = /\$__([a-zA-Z]+)\(([^\)]*)\)/;
                    var sql = target.rawSql;
                    while (marcroRegex.test(sql)) {
                        var match = sql.match(marcroRegex);
                        if (match[1] == 'timeFilter') {
                            var upper = "'" + this.range.to.toISOString() + "'";
                            var lower = "'" + this.range.from.toISOString() + "'";
                            sql = sql.replace(match[0], "(" + match[2] + " BETWEEN " + lower + " AND " + upper + ")");
                        }
                        else if (match[1] == 'timeFrom') {
                            sql = sql.replace(match[0], "'" + this.range.from.toISOString() + "'");
                        }
                        else if (match[1] == 'timeTo') {
                            sql = sql.replace(match[0], "'" + this.range.to.toISOString() + "'");
                        }
                        else if (match[1] == 'unixEpochFrom') {
                            sql = sql.replace(match[0], "" + this.range.from.unix());
                        }
                        else if (match[1] == 'unixEpochTo') {
                            sql = sql.replace(match[0], "" + this.range.to.unix());
                        }
                        else if (match[1] == 'unixEpochMilliFrom') {
                            sql = sql.replace(match[0], "" + this.range.from.valueOf());
                        }
                        else if (match[1] == 'unixEpochMilliTo') {
                            sql = sql.replace(match[0], "" + this.range.to.valueOf());
                        }
                        else {
                            sql = sql.replace(match[0], '');
                        }
                    }
                    sql = sql.replace(/[\r\n\t]/g, ' ');
                    return sql;
                };
                return OrientDBQuery;
            })();
            exports_1("default", OrientDBQuery);
        }
    }
});
//# sourceMappingURL=orientdb_query.js.map