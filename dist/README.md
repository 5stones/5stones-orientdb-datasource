## OrientDB Data Source For Grafana

This plugin provides an OrientDB datasource for Grafana.


#### Development

Run grafana in docker with a mapped volume to store your db state:

```
docker run \
  -d \
  -p 3000:3000 \
  --name=grafana \
  --volume=/your/folder/path/grafana-storage:/var/lib/grafana \
  -e "GF_DATAPROXY_LOGGING=true" \
  -e "GF_LOG_LEVEL=debug" \
  -e "GF_SERVER_ROUTER_LOGGING=true" \
  grafana/grafana
```

Clone the project to the grafana plugins directory:

```
git clone git@gitlab.com:5stones/5stones-orientdb-datasource.git /your/folder/path/grafana-storage/plugins/
```

Build and watch the plugin files with:

```
cd /your/folder/path/grafana-storage/plugins/5stones-orientdb-datasource
npm run build:watch
```
