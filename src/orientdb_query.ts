import _ from 'lodash';

export default class OrientDBQuery {
  target: any;
  range: any;
  templateSrv: any;
  scopedVars: any;

  /** @ngInject */
  constructor(target, range?, templateSrv?, scopedVars?) {
    this.target = target;
    this.range = range;
    this.templateSrv = templateSrv;
    this.scopedVars = scopedVars;

    target.format = target.format || 'time_series';
    target.timeColumn = target.timeColumn || 'time';
    target.metricColumn = target.metricColumn || 'none';

    target.group = target.group || [];
    target.where = target.where || [{ type: 'macro', name: '$__timeFilter', params: [] }];
    target.select = target.select || [[{ type: 'column', params: ['value'] }]];

    // handle pre query gui panels gracefully
    if (!('rawQuery' in this.target)) {
      if ('rawSql' in target) {
        // pre query gui panel
        target.rawQuery = true;
      } else {
        // new panel
        target.rawQuery = false;
      }
    }

    // give interpolateQueryStr access to this
    this.interpolateQueryStr = this.interpolateQueryStr.bind(this);
  }

  public quoteLiteral(value) {
    return "'" + value.replace(/'/g, "''") + "'";
  }

  public escapeLiteral(value) {
    return String(value).replace(/'/g, "''");
  }

  public interpolateQueryStr(value, variable, defaultFormatFn) {
    // if no multi or include all do not regexEscape
    if (!variable.multi && !variable.includeAll) {
      return this.escapeLiteral(value);
    }

    if (typeof value === 'string') {
      return this.quoteLiteral(value);
    }

    const escapedValues = _.map(value, this.quoteLiteral);
    return escapedValues.join(',');
  }

  public render(interpolate?) {
    const target = this.target;

    if (interpolate) {
      let sql = this._parseMacros(target, this.scopedVars);
      return this.templateSrv.replace(sql, this.scopedVars, this.interpolateQueryStr);
    } else {
      return target.rawSql;
    }
  }

  private _parseMacros(target, scopedVars): string {
    let marcroRegex = /\$__([a-zA-Z]+)\(([^\)]*)\)/;
    let sql = target.rawSql;

    while (marcroRegex.test(sql)) {
      let match = sql.match(marcroRegex);

      if (match[1] == 'timeFilter') {
        let upper = `'${this.range.to.toISOString()}'`;
        let lower = `'${this.range.from.toISOString()}'`;
        sql = sql.replace(match[0], `(${match[2]} BETWEEN ${lower} AND ${upper})`);
      } else if (match[1] == 'timeFrom') {
        sql = sql.replace(match[0], `'${this.range.from.toISOString()}'`);
      } else if (match[1] == 'timeTo') {
        sql = sql.replace(match[0], `'${this.range.to.toISOString()}'`);
      } else if (match[1] == 'unixEpochFrom') {
        sql = sql.replace(match[0], `${this.range.from.unix()}`);
      } else if (match[1] == 'unixEpochTo') {
        sql = sql.replace(match[0], `${this.range.to.unix()}`);
      } else if (match[1] == 'unixEpochMilliFrom') {
        sql = sql.replace(match[0], `${this.range.from.valueOf()}`);
      } else if (match[1] == 'unixEpochMilliTo') {
        sql = sql.replace(match[0], `${this.range.to.valueOf()}`);
      } else {
        sql = sql.replace(match[0], '');
      }
    }

    sql = sql.replace(/[\r\n\t]/g, ' ');

    return sql;
  }
}
