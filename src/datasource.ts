///<reference path='../node_modules/grafana-sdk-mocks/app/headers/common.d.ts' />

import _ from 'lodash';
import ResponseParser from './response_parser';
import OrientDBQuery from './orientdb_query';

export default class OrientDBDatasource {
  id: number;
  name: string;
  baseUrl: string;
  database: string;
  responseParser: ResponseParser;
  queryModel: OrientDBQuery;

  /** @ngInject */
  constructor(instanceSettings, private backendSrv, private templateSrv, private $q) {
    this.name = instanceSettings.name;
    this.id = instanceSettings.id;
    this.baseUrl = '/api/datasources/proxy/' + this.id + '/orientdb/';
    this.database = instanceSettings.jsonData.database;

    this.queryModel = new OrientDBQuery({});
    this.responseParser = new ResponseParser(this.$q);
  }

  interpolateVariable = (value, variable) => {
    if (typeof value === 'string') {
      if (variable.multi || variable.includeAll) {
        return this.queryModel.quoteLiteral(value);
      } else {
        return value;
      }
    }

    if (typeof value === 'number') {
      return value;
    }

    const quotedValues = _.map(value, v => {
      return this.queryModel.quoteLiteral(v);
    });
    return quotedValues.join(',');
  };

  private _getSqlEndpoint(): string {
      return this.baseUrl + 'command/' + this.database + '/sql';
  }

  public query(options) {
    const queries = _.filter(options.targets, target => {
      return target.hide !== true;
    }).map(target => {
      const queryModel = new OrientDBQuery(target, options.range, this.templateSrv, options.scopedVars);
      return queryModel.render(this.interpolateVariable)
    });

    if (queries.length === 0) {
      return this.$q.when({ data: [] });
    }

    return this.backendSrv
      .datasourceRequest({
        url: this._getSqlEndpoint(),
        method: 'POST',
        data: queries[0],
      })
      .then((results) => {
        return this.responseParser.processQueryResult(results);
      })
    ;
  }

  public annotationQuery(options) {
    throw new Error('Annotation Support not implemented yet.');
  }

  public metricFindQuery(query: string) {
    throw new Error('Metric Find Support not implemented yet.');
  }

  public testDatasource() {
    return this.backendSrv
      .datasourceRequest({
        url: this._getSqlEndpoint(),
        method: 'POST',
        data: 'SELECT 1',
      })
      .then(res => {
        return { status: 'success', message: 'Database Connection OK' };
      })
      .catch(err => {
        console.log(err);
        if (err.data && err.data.message) {
          return { status: 'error', message: err.data.message };
        } else {
          return { status: 'error', message: err.status };
        }
      })
    ;
  }
}
