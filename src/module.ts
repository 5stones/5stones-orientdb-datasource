import OrientDBDatasource from './datasource';
import {OrientDBQueryCtrl} from './query_ctrl';
import {OrientDBConfigCtrl} from './config_ctrl';

class OrientDBAnnotationsQueryCtrl {
  static templateUrl = 'partials/annotations.editor.html';
}

export {
  OrientDBDatasource as Datasource,
  OrientDBQueryCtrl as QueryCtrl,
  OrientDBConfigCtrl as ConfigCtrl,
  OrientDBAnnotationsQueryCtrl as AnnotationsQueryCtrl,
};
