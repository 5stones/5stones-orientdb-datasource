///<reference path="../node_modules/grafana-sdk-mocks/app/headers/common.d.ts" />

export class OrientDBConfigCtrl {
  static templateUrl = 'partials/config.html';
  current: any;

  constructor($scope) {
  }

  public refreshToken(): void {
    this.current.jsonData.token = btoa(this.current.jsonData.user + ':' + this.current.jsonData.password);
    console.log(this.current.jsonData.user, this.current.jsonData.password, this.current.jsonData.token);
  }
}
